using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sideWall : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;
    public playerControl player;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D anotherCollider)
    {
        if (anotherCollider.name == "ball")
        {
            player.incrementScore();

            if (player.Score < gameManager.maxScore)
            {
                anotherCollider.gameObject.SendMessage("restartGame", 2.0f, SendMessageOptions.RequireReceiver);
            }
        }
    }
}
